import org.junit.Assert;
import org.junit.Test;

public class RoverTest {

    @Test
    public void testPositionInitial(){
        Rover rover = new Rover();
        int[] positionXY = rover.getPositionRover();
        Assert.assertEquals(1,positionXY[0]);
        Assert.assertEquals(1,positionXY[1]);
    }
    @Test
    public void testDirectionInitial(){
        Rover rover = new Rover();
        char orientation = rover.getOrientation();
        Assert.assertEquals('N',orientation);
    }

    @Test
    public void testGestionCommandeSousFormeArray(){
        //Given
        Rover rover = new Rover();
        char[] commandesEntree = {'F', 'G', 'F', 'F', 'B', 'D'};

        //When
        rover.gestionCommande(commandesEntree);
        int[] positionXY = rover.getPositionRover();
        char orientation = rover.getOrientation();

        //Then
        Assert.assertEquals('N', orientation);
        Assert.assertEquals(5, positionXY[0]);
        Assert.assertEquals(2, positionXY[1]);
    }

    @Test
    public void testDepassementLimite(){
        //Given
        Rover rover = new Rover();
        char[] commandesEntree = {'F', 'F', 'F', 'F', 'F'};

        //When
        rover.gestionCommande(commandesEntree);
        int[] positionXY = rover.getPositionRover();
        char orientation = rover.getOrientation();

        //Then
        Assert.assertEquals('N', orientation);
        Assert.assertEquals(1, positionXY[0]);
        Assert.assertEquals(1, positionXY[1]);
    }

//    @Test
//    public void testDeplacementVersAvant(){
//        Rover rover = new Rover();
//        rover.deplacer('F');
//        int[] positionXY = rover.getPositionRover();
//        Assert.assertEquals(0,positionXY[0]);
//        Assert.assertEquals(1,positionXY[1]);
//    }
//
//    @Test
//    public void testDeplacementVersArriere(){
//        Rover rover = new Rover();
//        rover.deplacer('B');
//        int[] positionXY = rover.getPositionRover();
//        Assert.assertEquals(-1,positionXY[1]);
//        Assert.assertEquals(0,positionXY[0]);
//    }
//
//    @Test
//    public void testTournerGauche(){
//        //Given
//        Rover rover = new Rover();
//        //When
//        rover.tourner('G');
//        char orientation = rover.getOrientation();
//        //Then
//        Assert.assertEquals('O',orientation);
//    }
//    @Test
//    public void testTournerDroite(){
//        //Given
//        Rover rover = new Rover();
//        //When
//        rover.tourner('D');
//        char orientation = rover.getOrientation();
//        //Then
//        Assert.assertEquals('E',orientation);
//    }
//    @Test
//    public void deplacementMultiple(){
//
//        //Given
//        Rover rover = new Rover();
//
//        //When
//        rover.tourner('D');
//        rover.deplacer('F');
//        rover.deplacer('F');
//        rover.tourner('G');
//        rover.deplacer('F');
//        rover.deplacer('F');
//        int[] positionXY = rover.getPositionRover();
//        char orientation = rover.getOrientation();
//
//        //Then
//        Assert.assertEquals('N',orientation);
//        Assert.assertEquals(2,positionXY[1]);
//        Assert.assertEquals(2,positionXY[0]);
//    }

}
